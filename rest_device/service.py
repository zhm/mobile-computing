import yaml
import uuid
from threading import Thread
from communication import PubSub
class Service(PubSub, Thread):
    def __init__(self, uuid, configuration, deviceState, host="127.0.0.1", port="8080"):
        super(Service, self).__init__(host=host)
        Thread.__init__(self)
        self.configuration = configuration
        self.uuid = uuid
        self.ds = deviceState
        self.channelSuffix = self.ds.getChannelSuffix()
        self.port=port

    def run(self):
        # Implemented by the concrete class
        pass
