import uuid
import yaml
import json
import time
from communication import PubSub
from service import Service
from device_state import DeviceState
from datetime import datetime,timedelta
import netifaces as ni
from sys import platform as _platform
from flask import Flask, request, g
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
app.config['PROPAGATE_EXCEPTIONS'] = True
api = Api(app)
class NotFound(Exception):
    status_code = 404

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(NotFound)
def handle_not_found(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

import uuid
import yaml
import json
class Device(Resource):
	def __init__(self, services, deviceState):
		self.services = services
		self.ds = deviceState
		super(Device, self).__init__()

	def get(self):
		"""
			Return full list of services, modelInfo, etc. Return all current
			state and configuration
		"""
		return json.dumps(self.ds.getAll())


class APIServiceList(Resource):
	def __init__(self, deviceState):
		self.ds = deviceState

	def get(self):
		return self.ds.getAllServices()

class APIService(Resource):
	def __init__(self, deviceState):
		self.ds = deviceState

	def get(self, service_id):
		service = self.ds.getServiceById(service_id)
		if service == None:
			raise NotFound("Service with id {} does not exist".format(service_id))
		return service
        
class RegistryService(Service):
    """ Periodically update the registry with our ip address """
    def run(self):
        channel = "registry"
        while True:
            # Check the information
            ip = getIp()
            message = {
                "id" : self.ds.getDeviceId(),
                "status" : self.ds.getStatus(),
                "ip" : ip+":"+self.port
            }
            print message
            # Publish every 5 minutes with new information
            self.publish(channel, json.dumps(message))
            time.sleep(5*60)
            
import requests             
class ApplianceService(Service):
    """ submit tasks to scheduler"""
    def run(self): 
        channel = "-".join([self.uuid, self.channelSuffix])
        print channel
        data = {}
        data["deviceId"] = self.ds.getDeviceId()
        data["duration"] = (1,0)
        data["dependencies"] = ["LG-0001WASHER-1"]
        time.sleep(10)
        self.publish(channel, json.dumps(data))
        
        r = requests.get('http://128.135.116.20:8080/registry/')
        devices = r.json()
        for device in devices:
            if "University" in device["id"]:
                r = requests.get("http://" + device["ip"] + "/device/")
                services = json.loads(r.json())["services"]["high"]
                for service in services:
                    if service["serviceType"] == "scheduler":
                        model=json.loads(r.json())["modelInfo"]
                        chan = str("-".join([service["id"],model["manufacturer"],
                                       model["modelNumber"],
                                       str(model["serialNumber"])]))
                        print "subscribing to: " + chan
                        self.subscribe(chan)
        #self.subscribe("d20bbaa1-85b3-4003-86cd-95445f2308fe-The University of Chicago-0001VD-1")
        while True: 
            for message in self.listen():
                print message["data"]
def getIp():
    if _platform == "linux" or _platform == "linux2":
        netData = ni.ifaddresses('wlan0')
    elif _platform == "win32":
        netData = ni.ifaddresses('{F6101E17-AECB-4341-976B-EE5E5B956AAB}')            
    return netData[2][0]["addr"]
def main():
    ds = DeviceState("configDryer.yaml")
    services = ds.getLowServices()
    serviceList = []
    for sID in services.keys():
        if services[sID]["serviceType"] == "appliance":
            ss = ApplianceService(sID, services[sID], ds)
            serviceList.append(ss)
            ss.start()
        elif services[sID]["serviceType"] == "registry":
            rs = RegistryService(sID, services[sID], ds, host="128.135.116.20",port="8083")
            serviceList.append(rs)
            rs.start()
    api.add_resource(Device, '/device/', resource_class_args=(serviceList, ds))
    api.add_resource(APIServiceList, '/device/service/', resource_class_args=(ds,))
    api.add_resource(APIService, '/device/service/<service_id>/', resource_class_args=(ds,))
           
if __name__ == "__main__":
    main()
    app.run(host=getIp(), port=8083)