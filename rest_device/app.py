from flask import Flask, request, g
from flask_restful import Resource, Api, reqparse
import svc_config as config

app = Flask(__name__)
app.config['PROPAGATE_EXCEPTIONS'] = True
api = Api(app)

from flask import jsonify
class NotFound(Exception):
    status_code = 404

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(NotFound)
def handle_not_found(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

import uuid
import yaml
import json
class Device(Resource):
	def __init__(self, services, deviceState):
		self.services = services
		self.ds = deviceState
		super(Device, self).__init__()

	def get(self):
		"""
			Return full list of services, modelInfo, etc. Return all current
			state and configuration
		"""
		return json.dumps(self.ds.getAll())


class APIServiceList(Resource):
	def __init__(self, deviceState):
		self.ds = deviceState

	def get(self):
		return self.ds.getAllServices()

class APIService(Resource):
	def __init__(self, deviceState):
		self.ds = deviceState

	def get(self, service_id):
		service = self.ds.getServiceById(service_id)
		if service == None:
			raise NotFound("Service with id {} does not exist".format(service_id))
		return service

import time
from communication import PubSub
from service import Service
class MetronomeService(Service):
    """
    Post messages at the rate specified in the configuration file
    """
    def run(self):
        channel = "-".join([self.uuid, self.channelSuffix])
        while True:
            # Publish on frequency
            time.sleep(1/self.configuration["settings"]["freq"])
            self.publish(channel, "Metronome!")

import netifaces as ni
from sys import platform as _platform
class RegistryService(Service):
    """ Periodically update the registry with our ip address """
    def run(self):
        channel = "registry"
        while True:
            # Check the information
            if _platform == "linux" or _platform == "linux2":
                netData = ni.ifaddresses('wlan0')
            elif _platform == "win32":
                netData = ni.ifaddresses('{F6101E17-AECB-4341-976B-EE5E5B956AAB}')
            
            ip = netData[2][0]["addr"]
            message = {
                "id" : self.ds.getDeviceId(),
                "status" : self.ds.getStatus(),
                "ip" : ip+":"+self.port
            }
            print message
            # Publish every 5 minutes with new information
            self.publish(channel, json.dumps(message))
            time.sleep(5*60)
            
from toposort import toposort
from datetime import datetime, timedelta
import requests
from sets import Set
class SchedulerService(Service):
    """ Output optimal schedule for given tasks """
    def getTasks(self):
        devices = []
        while True: 
            for message in self.listen():
                #print message["data"]
                task = json.loads(message["data"])
                duration = task["duration"]
                task["duration"] = timedelta(hours=duration[0], minutes = duration[1])
                if "dependencies" in task:
                    dep = task["dependencies"]
                    task["dependencies"] = set(dep)
                devices.append(task)
                if(len(devices) == 2):
                    return devices
    def run(self): 
        '''d1 = {"washer"}
        d2 = {"load1"}
        d3 = {"load1","load2"}
        washer = {"deviceId": "washer", "duration":timedelta(hours=1, minutes=30)}
        dryer = {"deviceId": "dryer", "duration":timedelta(hours=1), "dependencies":d1}
        load1 = {"deviceId": "load1", "duration":timedelta(minutes=35)}
        load2 = {"deviceId": "load2", "duration":timedelta(hours=2), "dependencies":d2}
        load3 = {"deviceId": "load3", "duration":timedelta(minutes=22), "dependencies":d3}'''
        #self.subscribe("b863f445-7599-465f-bfa1-e757bfd6402b-LG-0001WASHER-1")
        #self.subscribe("33439621-05a3-40ad-b21f-16b1e2bf0a4a-LG-0001DRYER-1")
        r = requests.get('http://128.135.116.20:8080/registry/')
        devices = r.json()
        numDevices = 0
        for device in devices:
            if "WASH" in device["id"] or "DRY" in device["id"]:
                r = requests.get("http://" + device["ip"] + "/device/")
                services = json.loads(r.json())["services"]["low"]
                for service in services:
                    if service["serviceType"] == "appliance":
                        model=json.loads(r.json())["modelInfo"]
                        chan = str("-".join([service["id"],model["manufacturer"],
                                       model["modelNumber"],
                                       str(model["serialNumber"])]))
                        print "subscribing to: " + chan
                        numDevices += 1
                        self.subscribe(chan)
            
        channel = "-".join([self.uuid, self.channelSuffix])
        devices = self.getTasks()         
        #devices = [washer,dryer,load1,load2,load3]
        dependencies = {}
        durations = {}
        durationSum = timedelta(hours=0)
        for device in devices:
            durations[device["deviceId"]] = device["duration"]
            durationSum += device["duration"]
            if "dependencies" in device.keys():
                dependencies[device["deviceId"]] = device["dependencies"]
        sort = list(toposort(dependencies))
        start = datetime(1,1,1,9,0,0)
        end = datetime(1,1,1,17,0,0)
        numTasks = len(devices)
        nextTime = start
        leftover = (end-start)-durationSum
        spacing = leftover/numTasks
        schedule = {}
        for set in sort:
            for task in set:
                print task, nextTime, durations[task]
                schedule[task] = (nextTime.hour,nextTime.minute)
                nextTime += durations[task]+spacing
        time.sleep(5)
        self.publish(channel, json.dumps(schedule))
        
        
import urllib2, urllib
from datetime import datetime
import os
from apiclient.discovery import build
from httplib2 import Http
import oauth2client
from oauth2client import client
from oauth2client import tools
import pyglet
import random

'''try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None
'''    
class RadioService(Service):
    """ Pick music to play based on weather and calendar """
    
    def get_credentials(self):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        SCOPES = 'https://www.googleapis.com/auth/calendar.readonly'
        CLIENT_SECRET_FILE = 'client_secret.json'
        APPLICATION_NAME = 'Google Calendar API Quickstart'
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,
                                       'calendar-quickstart.json')

        store = oauth2client.file.Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatability with Python 2.6
                credentials = tools.run(flow, store)
            print 'Storing credentials to ' + credential_path
        return credentials

    def inputToMood(self, temp, condition, calendar):
        t = False
        goodConditions = [27,28,29,30,31,32,33,34]
        if (temp >= 41 and temp <= 70):
            t = True
        c = int(condition) in goodConditions
        hour = datetime.now().hour
        
        if calendar == "workout":
            return "pumped"
        elif calendar == "date":
            return "love"
        elif calendar == "study":
            return "sad"
        
        if hour >= 6 and hour <= 10:
            return "pumped"
        if hour >= 22 and hour <= 24:
            return "sad"
        
        if t and c:
            return "happy"
        elif t:
            return "anxious"
        elif c:
            return "confused"
        else: 
            return "sad"
            
    def getWeather(self):
        baseurl = "https://query.yahooapis.com/v1/public/yql?"
        yql_query = "select item.condition from weather.forecast where woeid=2379574"
        yql_url = baseurl + urllib.urlencode({'q':yql_query}) + "&format=json"
        result = urllib2.urlopen(yql_url).read()
        data = json.loads(result)
        temp =  int(data['query']['results']["channel"]["item"]["condition"]["temp"])
        text =  data['query']['results']["channel"]["item"]["condition"]["text"]
        condition =  data['query']['results']["channel"]["item"]["condition"]["code"]
        return (temp,text,condition)
    
    def getCalendar(self):
        credentials = self.get_credentials()
        service = build('calendar', 'v3', http=credentials.authorize(Http()))

        now = datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        eventsResult = service.events().list(
            calendarId='primary', timeMin=now, maxResults=1, singleEvents=True,
            orderBy='startTime').execute()
        events = eventsResult.get('items', [])
        if len(events) > 0:
            event = events[0]
            start = event['start'].get('dateTime', event['start'].get('date'))
            return str.lower(str(event['summary']))
        else: 
            return ""

    def playMusic(self, genre):
        files = os.listdir("songs/"+genre)
        if files is None or len(files) == 0:
            music = pyglet.resource.media('short.wav')
        else:
            music = pyglet.resource.media("songs/"+genre+"/"+random.choice(files))
        #music.play()
        self.player.queue(music)
        if(self.player.playing):
            self.player.next()
        self.player.play()
        time.sleep(10)
        #time.sleep(int(music.duration)+1)
        
    def run(self):
        self.player = pyglet.media.Player()
        channel = "radio"
        map = {}
        map["sad"] = "classical"
        map["happy"] = "rock"
        map["anxious"] = "hip-hop"
        map["confused"] = "hip-hop"
        map["love"] = "soul"
        map["pumped"] = "uptempo rock"
        
        while True:
            temp,text,condition = self.getWeather()
            calendar = self.getCalendar()
            mood = self.inputToMood(temp, condition, calendar)
            out = map[mood]
            print "in: " + str(temp) + " " + text + " " + calendar + " " + str(datetime.now().hour)
            print "out: " + mood + "->" + out
            self.playMusic(out)
            self.publish(channel, out)
def getIp():
    if _platform == "linux" or _platform == "linux2":
        netData = ni.ifaddresses('wlan0')
    elif _platform == "win32":
        netData = ni.ifaddresses('{F6101E17-AECB-4341-976B-EE5E5B956AAB}')            
    return netData[2][0]["addr"]
    
from device_state import DeviceState
from communication import Broker
from multiprocessing import Process
def main(demo_service):
    ds = DeviceState("config.yaml")
    services = ds.getLowServices()
    highServices = ds.getHighServices()
    serviceList = []
    processes = []
    newProcess = Process(target=Broker)
    processes.append(newProcess)
    newProcess.start()
    for sID in services.keys():
        '''if services[sID]["serviceType"] == "metronome":
            # Register a metronome service
            ms = MetronomeService(sID, services[sID], ds)
            serviceList.append(ms)
            ms.start()'''
        if services[sID]["serviceType"] == "registry":
            rs = RegistryService(sID, services[sID], ds, host="128.135.116.20")
            serviceList.append(rs)
            rs.start()
    for sID in highServices.keys():
        if demo_service == "scheduler":
            if highServices[sID]["serviceType"] == "scheduler":
                ss = SchedulerService(sID, highServices[sID], ds)
                serviceList.append(ss)
                ss.start()
        elif demo_service == "radio":
            if highServices[sID]["serviceType"] == "radio":
                rs = RadioService(sID, highServices[sID], ds)
                serviceList.append(rs)
                rs.start()
    api.add_resource(Device, '/device/', resource_class_args=(serviceList, ds))
    api.add_resource(APIServiceList, '/device/service/', resource_class_args=(ds,))
    api.add_resource(APIService, '/device/service/<service_id>/', resource_class_args=(ds,))

import argparse
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("demo_service",type=str,help="radio or scheduler")
    args = parser.parse_args()
    main(args.demo_service)
    app.run(host = getIp(), port=config.SVC_PORT, debug=config.DEBUG_MODE)
    #pyglet.app.run()
   