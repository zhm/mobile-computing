CODE = {'A': '.-',     'B': '-...',   'C': '-.-.',
         'D': '-..',    'E': '.',      'F': '..-.',
         'G': '--.',    'H': '....',   'I': '..',
         'J': '.---',   'K': '-.-',    'L': '.-..',
         'M': '--',     'N': '-.',     'O': '---',
         'P': '.--.',   'Q': '--.-',   'R': '.-.',
         'S': '...',    'T': '-',      'U': '..-',
         'V': '...-',   'W': '.--',    'X': '-..-',
         'Y': '-.--',   'Z': '--..',

         '0': '-----',  '1': '.----',  '2': '..---',
         '3': '...--',  '4': '....-',  '5': '.....',
         '6': '-.',  '7': '--',  '8': '---..',
         '9': '----.'
         }

def encode(message):
     ret = ""
     for char in message:
             ret += CODE[char.upper()]
     return ret

import requests
import json
import time
def sendToArduino(encoded):
     for code in encoded:
             if code == '.':
                     requests.post("http://128.135.116.29/device/services/1/settings/", data=json.dumps({"name" : "brightness", "currentValue" : 255}))
                     time.sleep(0.2)
                     requests.post("http://128.135.116.29/device/services/1/settings/", data=json.dumps({"name" : "brightness", "currentValue" : 0}))
             elif code == '-':
                     requests.post("http://128.135.116.29/device/services/1/settings/", data=json.dumps({"name" : "brightness", "currentValue" : 255}))
                     time.sleep(0.8)
                     requests.post("http://128.135.116.29/device/services/1/settings/", data=json.dumps({"name" : "brightness", "currentValue" : 0}))


encoded = encode("HelloWorld")
sendToArduino(encoded)
