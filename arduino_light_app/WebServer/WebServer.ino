/*
  Web Server
 
 A simple web server that shows the value of the analog input pins.
 using an Arduino Wiznet Ethernet shield. 
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 * Analog inputs attached to pins A0 through A5 (optional)
 
 created 18 Dec 2009
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe
 
 */

#include <SPI.h>
#include <EthernetV2_0.h>
#include <ArduinoJson.h>

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(128,135,116, 29);

// Initialize the Ethernet server library
// with the IP address and port you want to use 
// (port 80 is default for HTTP):
EthernetServer server(80);
#define W5200_CS  10
#define SDCARD_CS 4
#define ISGET(X) (X.substring(0,3) == "GET")

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);  
  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
  pinMode(W5200_CS, OUTPUT);
  //disconnect the W5200
  digitalWrite(W5200_CS,HIGH);
  
  // disconnect the SD card
  digitalWrite(SDCARD_CS, HIGH);
  
  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
}

void printHeader(EthernetClient client){
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: application/json");
  client.println("Connnection: close");
  client.println();
}

  void printAll(EthernetClient client){

}

void sendModelInfo(EthernetClient client){
  // Generate the JSON string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["manufacturer"] = "The University of Chiago";
  root["modelNumber"] = "001ARD";
  root["serialNumber"] = 1;
  root.printTo(client);
}

void listServices(EthernetClient client){
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonArray& data =  root.createNestedArray("services");
  JsonObject& service = data.createNestedObject();
  service["name"] = "LightService";
}

void changeServiceSettings(EthernetClient client, JsonObject settings){
  
}

void parseRequest(EthernetClient client, char *request, char *json){
  boolean currentLineIsBlank = false;
  boolean firstLine = true;
  boolean appendToJson = false;
  int i = 0;
  int j = 0;
  // Parse the type of request
  while(client.available()){
    char c = client.read();
    if (firstLine && c == '\n') firstLine = false;
    else if (firstLine){
      request[i++] = c;
    }
    else if (currentLineIsBlank && c == '\n'){
      appendToJson = true;
    }
    else if (c == '\n') currentLineIsBlank = true;
    else if (appendToJson){
      json[j++] = c;
    }
    else if (c != '\r') currentLineIsBlank = false;
  }
  request[i] = '\0';
  json[j] = '\0';
}

void loop() {  
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    char request[100];
    char json[300];
    while (client.connected()) {
      parseRequest(client, request, json);
      if (!strncmp(request, "GET", 3)){
        // Dispatch the GET request by chopping up the request
        // Ignore the first half
        strtok(request, "/");
        char *next = strtok(NULL, "/");
        Serial.println(next);
        if (!strcmp(next, "device")){
           // Check if another token exists 
           printHeader(client);
           printAll(client);
           if (!strcmp(strtok(NULL, "/"), "services")){
             
           }
        }
        
      }
      Serial.println(request);
      Serial.println(strlen(json));
      break;
      
    }
    
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disonnected");
  }
}

