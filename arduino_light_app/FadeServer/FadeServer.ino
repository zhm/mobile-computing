// -*- c++ -*-
//
// Copyright 2010 Ovidiu Predescu <ovidiu@gmail.com>
// Date: June 2010
// Updated: 08-JAN-2012 for Arduno IDE 1.0 by <Hardcore@hardcoreforensics.com>
//

#include <EthernetV2_0.h>
#include <SPI.h>
#include <Flash.h>
#include <SD.h>
#include <TinyWebServer.h>
#include <ArduinoJson.h>


/****************VALUES YOU CHANGE*************/
#define W5200_CS  10
#define SDCARD_CS 4
#define LED1 5
#define LED2 6
#define LED3 9

// Don't forget to modify the IP to an available one on your home network
byte ip[] = { 128, 135, 116, 29};
byte server[] = { 128, 135, 116, 20 };

File myFile;

uint8_t brightness = 0;
/*********************************************/

static uint8_t mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

TinyWebServer::PathHandler handlers[] = {
  {"/device/", TinyWebServer::GET, &sendAll},
  {"/device/modelInfo/", TinyWebServer::GET, &sendModInfo},
  {"/device/services/", TinyWebServer::GET, &sendServices},
  {"/device/services/1/settings/", TinyWebServer::GET, &sendSettings},
  {"/device/services/1/settings/", TinyWebServer::POST, &device_handler },
  {NULL},
};

boolean sendSettings(TinyWebServer& web_server) {
  web_server.send_error_code(200);
  web_server.send_content_type("application/json");
  web_server.end_headers();
  web_server << F("{\"name\" : \"brightness\", \"currentValue\" : ") << brightness << F(", \"max\" : 255, \"min\" : 0}\n");
  return true;  
}

boolean sendServices(TinyWebServer& web_server) {
  send_file(web_server, "service.jsn");
  return true;
}

boolean sendModInfo(TinyWebServer& web_server) {
  send_file(web_server, "modInfo.jsn");
  return true;
}

boolean sendAll(TinyWebServer& web_server) {
  send_file(web_server, "config.jsn");
  return true;
}

boolean send_file(TinyWebServer &web_server, char *filename) {
  web_server.send_error_code(200);
  web_server.send_content_type("application/json");
  web_server.end_headers();
  myFile = SD.open(filename);
  while (myFile.available()) {
    //send the content onto the serial monitor
    web_server << (char)myFile.read();
  }
  // close the file
  myFile.close();
}

boolean device_handler(TinyWebServer& web_server) {
  web_server.send_error_code(200);
  web_server.end_headers();

  EthernetClient client = web_server.get_client();
  char json[100];
  int i = 0;
  while (client.available()) {
    char c = client.read();
    json[i++] = c;
    if (c == '}') {
      break;
    }
  }
  // Parse the input
  StaticJsonBuffer<100> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(json);

  // Verify input is correct and act accordingly
  const char *name = root["name"];
  if (!strcmp(name, "brightness")) {
    int currentValue = root["currentValue"];
    if (currentValue <= 255 && currentValue >= 0) {
      brightness = currentValue;
      setBrightness();
    }
  }

  return true;
}

TinyWebServer web = TinyWebServer(handlers, NULL);

void setup() {
  Serial.begin(115200);
  Serial << F("Free RAM: ") << FreeRam() << "\n";

  pinMode(SS_PIN, OUTPUT);	// set the SS pin as an output
  // (necessary to keep the board as
  // master and not SPI slave)
  digitalWrite(SS_PIN, HIGH);	// and ensure SS is high

  // Ensure we are in a consistent state after power-up or a reset
  // button These pins are standard for the Arduino w5100 Rev 3
  // ethernet board They may need to be re-jigged for different boards
  pinMode(W5200_CS, OUTPUT);	// Set the CS pin as an output
  digitalWrite(W5200_CS, HIGH);	// Turn off the W5100 chip! (wait for
  // configuration)

  if (!SD.begin(SDCARD_CS)) {
    return;
  }
  pinMode(SDCARD_CS, OUTPUT);	// Set the SDcard CS pin as an output
  digitalWrite(SDCARD_CS, HIGH);	// Turn off the SD card! (wait for
  // configuration)

  // Initialize the Ethernet.
  Ethernet.begin(mac, ip);
  EthernetClient client;
  if (client.connect(server, 7000)) {
    Serial.println("connected");
    // send the HTTP PUT request:
    client.write((uint8_t)1);
    client.write((uint8_t)0);
    client.write((uint8_t)(100 + 1));
    client.write((uint8_t)0);

    // here's the actual content of the PUT request:
    client.print(F("registry|||{\"id\":\"Combustible Lemons-AutoLamp 5000-1\",\"status\":\"ONLINE\",\"ip\":\"128.135.116.29\"}"));
    client.flush();
    client.stop();
  } 

  // Start the web server.
  web.begin();
}

void setBrightness() {
  // set the brightness of pin 9:
  analogWrite(LED1, brightness);
  analogWrite(LED2, brightness);
  analogWrite(LED3, brightness);
}

void loop() {
  web.process();
}
