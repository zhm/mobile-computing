"""Implements a proxy that passes requests to services residing on a single device."""

__author__ = 'Justyn Harriman <justyn.harriman@gmail.com>'

import flask
from flask import request
import json
import requests
import yaml
from os import path
import sqlite3

import utils

CONFIG_DIR = path.expanduser('~/.bagend')
CONFIG_FILENAME = 'config.yaml'

node = flask.Flask(__name__)

@node.route('/status/', methods=['GET'])
def status():
    """Returns network status, including Broker ports"""
    status = {
        'status' : 'UP',
        'broker':{
            'publish_port': node.config['broker'].publish_port,
            'subscribe_port': node.config['broker'].subscribe_port,
            'url': node.config['broker'].url,
            'is_self': node.config['broker'].is_self
        }
    }
    return flask.jsonify(**status)
    
@node.route('/register/', methods=['POST'])
def register():
    """Registers a new device that this node will proxy to."""
    # Create UUID.
    
    
    # Add UUID to the database. 

def create_db(db_fname, schema_fname):
    with sqlite3.connect(dbname) as conn:
        cursor = conn.cursor()
        with open(schema_fname, 'r') as f:
            cursor.execute(f.read)
            conn.commit()
        

if __name__ == '__main__':
    # Load configuration file. 
    broker_data = utils.BrokerData.create_from_config(path.join(CONFIG_DIR, CONFIG_FILENAME))
    node.config['broker'] = broker_data
    
    # Create DB.
    create_db(path.join(CONFIG_DIR, 'node.db'), path.join(CONFIG_DIR, 'schema.sql'))
    
    node.run('0.0.0.0')
    
