__author__ = 'Justyn Harriman <justyn.harriman@gmail.com>'

import requests
import yaml

class BrokerData(object):
    def __init__(self, url=None, publish_port=None, subscribe_port=None, is_self=False):
        self.url = url
        self.publish_port = publish_port
        self.subscribe_port = subscribe_port
        self.is_self = is_self
    
    @classmethod
    def create_from_url(cls, url):
        """Creates a new BrokerData using a url to a Broker"""
        response_json = requests.get(url).json()
        info = response_json['broker']
        return cls(info['url'], info['publish_port'], info['subscribe_port'])
        
    @classmethod
    def create_from_config(cls, config_file):
        with open(config_file, 'r') as f:
            config = yaml.load(f)
        
        broker = config['broker']
        if broker['is_self']:
            return cls(broker['url'], broker['publish_port'], broker['subscribe_port'], True)
        elif broker['url']:
            return cls.create_from_url(broker['url'])
        else:
            return cls()
