__author__ = 'Justyn Harriman <justyn.harriman@gmail.com>'

DEFAULT_PUBLISH_PORT = 5557
DEFAULT_SUBSCRIBE_PORT = 5556
DEFAULT_HOST = '0.0.0.0'
TCP_FORMAT_STRING = 'tcp://{host}:{port}'

class Broker(object)
    def __init__(self, host=DEFAULT_HOST, publish_port=DEFAULT_PUBLISH_PORT, subscribe_port=DEFAULT_SUBSCRIBE_PORT):
        context = zmq.Context()
        # Towards publishers
        self.sub = context.socket(zmq.SUB)
        self.sub.bind(TCP_FORMAT_STRING.format(host=host, port=publish_port)
        self.sub.setsockopt_string(zmq.SUBSCRIBE, "")

        # Towards subscribers
        self.pub = context.socket(zmq.PUB)
        self.pub.bind(TCP_FORMAT_STRING.format(host=host, port=subscribe_port)
        zmq.device(zmq.FORWARDER, self.sub, self.pub)