import svc_config as config
import zmq

class Broker(object):
    def __init__(self, host=config.SVC_HOST):
        context = zmq.Context()
        # Towards publishers
        self.sub = context.socket(zmq.SUB)
        self.sub.bind("tcp://%s:5557" % host)
        self.sub.setsockopt_string(zmq.SUBSCRIBE, "")

        # Towards subscribers
        self.pub = context.socket(zmq.PUB)
        self.pub.bind("tcp://%s:5556" % host)
        zmq.device(zmq.FORWARDER, self.sub, self.pub)

class PubSub(object):
    def __init__(self, host=config.SVC_HOST):
        context = zmq.Context()
        self.pub = context.socket(zmq.PUB)
        self.pub.connect("tcp://%s:5557" % host)
        self.sub = context.socket(zmq.SUB)
        self.sub.connect("tcp://%s:5556" % host)

    def publish(self, channel, message):
        self.pub.send_unicode("%s|||%s" % (channel, message))

    def subscribe(self, channel):
        self.sub.setsockopt_string(zmq.SUBSCRIBE, channel)

    def unsubscribe(self, channel):
        self.sub.setsockopt(zmq.UNSUBSCRIBE, channel)

    def pubsub(self):
        return self

    def listen(self):
        while True:
            channel, _, data = self.sub.recv().decode('utf-8').partition("|||")
            yield {"type": "message", "channel": channel, "data": data}

class Rep(object):
    def __init__(self, host=config.SVC_HOST):
        context = zmq.Context()
        self.rep = context.socket(zmq.REP)
        self.rep.bind("tcp://%s:5558" % host)
        self.run()

    def run(self):
        while True:
            #  Wait for next request from client
            message = self.rep.recv()
            self.rep.send(self.handleDeviceRequest(message))

    def handleDeviceRegistration(self, message):
        pass

    def handleDeviceRequest(self, message):
        pass

class Req(object):
    def __init__(self, host=config.SVC_HOST):
        context = zmq.Context()
        self.req = context.socket(zmq.REQ)
        self.req.connect("tcp://%s:5558" % host)

    def send(self, message):
        self.req.send(message)
        #  Get the reply.
        return self.req.recv()


class Pull(object):
    def __init__(self, host=config.SVC_HOST):
        context = zmq.Context()
        self.pull = context.socket(zmq.PULL)
        self.pull.bind("tcp://%s:7000" % host)

    def listen(self):
        while True:
            message = self.pull.recv()
            yield message
