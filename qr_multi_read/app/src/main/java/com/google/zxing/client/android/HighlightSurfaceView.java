package com.google.zxing.client.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;

import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.camera.CameraManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jharriman on 5/24/15.
 */
public class HighlightSurfaceView extends SurfaceView {
    protected final Paint paint = new Paint();
    private float []mResultPoints;
    private Result []mResults;

    public HighlightSurfaceView(Context context) {
        super(context);
        setWillNotDraw(false);
        paint.setARGB(255, 200, 0, 0);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(2);
    }

    public HighlightSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mResultPoints = null;
        setWillNotDraw(false);
        paint.setARGB(255, 200, 0, 0);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(2);
    }

    public HighlightSurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setWillNotDraw(false);
        paint.setARGB(255, 200, 0, 0);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeWidth(2);
    }

    private CameraManager cameraManager;

    public void setCameraManager(CameraManager cameraManager) {
        this.cameraManager = cameraManager;
    }

    @Override
    protected void onDraw(Canvas canvas){
        if (cameraManager == null) {
            return; // not ready yet, early draw before done configuring
        }

        if (mResultPoints != null) {
            for (int i = 0; i < mResultPoints.length; i += 3){
                canvas.drawCircle((int) (mResultPoints[i]),
                        (int) (mResultPoints[i+1]),
                        mResultPoints[i+2], paint);

            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        final int action = event.getAction();
        if(action == MotionEvent.ACTION_DOWN) {
            for (int i = 0; i < mResultPoints.length; i += 3) {
                float centerX = mResultPoints[i];
                float centerY = mResultPoints[i + 1];
                float radius = mResultPoints[i + 2];
                if (Math.pow(x - centerX, 2) + Math.pow(y - centerY, 2) < Math.pow(radius, 2)) {
                    // Then the touch event was in this circle, display the data in an alertbox for now
                    Result clickedResult = mResults[i / 3];
                    Intent intent = new Intent(getContext(), ControlActivity.class);
                    intent.putExtra("url", clickedResult.getText());
                    getContext().startActivity(intent);
                    return true;
                }
            }
        }
        return false;

    }

    public void setResultPoints(Result []results){
        Rect frame = cameraManager.getFramingRect();
        Rect previewFrame = cameraManager.getFramingRectInPreview();

        float scaleX = frame.width() / (float) previewFrame.width();
        float scaleY = frame.height() / (float) previewFrame.height();
        int frameLeft = frame.left;
        int frameTop = frame.top;

        mResultPoints = new float[results.length * 3];
        for (int i = 0; i < results.length; i++){
            Result r = results[i];
            ResultPoint []set = r.getResultPoints();
            float xDist = ResultPoint.distance(set[0], set[1]) / 2;
            float yDist = ResultPoint.distance(set[0], set[2]) / 2;
            mResultPoints[i * 3] = frameLeft + (set[0].getX() + xDist) * scaleX;
            mResultPoints[(i * 3) + 1] = frameTop + (set[0].getY() - yDist) * scaleY;
            mResultPoints[(i * 3) + 2] = yDist * scaleY;
        }
        mResults = results;
        invalidate();
    }


}
