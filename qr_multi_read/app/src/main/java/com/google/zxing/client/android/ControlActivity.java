package com.google.zxing.client.android;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by jharriman on 5/25/15.
 */
public class ControlActivity extends Activity {

    private static class MySingleton{
        private static MySingleton mInstance;
        private RequestQueue mRequestQueue;
        private static Context mCtx;

        private MySingleton(Context context) {
            mCtx = context;
            mRequestQueue = getRequestQueue();
        }

        public static synchronized MySingleton getInstance(Context context) {
            if (mInstance == null) {
                mInstance = new MySingleton(context);
            }
            return mInstance;
        }

        public RequestQueue getRequestQueue() {
            if (mRequestQueue == null) {
                // getApplicationContext() is key, it keeps you from leaking the
                // Activity or BroadcastReceiver if someone passes one in.
                mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
            }
            return mRequestQueue;
        }

        public <T> void addToRequestQueue(Request<T> req) {
            getRequestQueue().add(req);
        }
    }

    private String mDeviceIp;

    private void requestDeviceInfo(final TextView temp, String deviceId){
        final Context context = this.getApplicationContext();
        // Acquire deviceIp object
        String url = String.format("http://stony-116-020.rh.uchicago.edu:8080/registry/%s/", deviceId);
        URL url1 = null;
        try {
            url1 = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        JsonArrayRequest jsObjRequest = new JsonArrayRequest
                (Request.Method.GET, url1.toString(), null, new Response.Listener<JSONArray>() {
                    @Override

                    public void onResponse(JSONArray response) {
                        // Make a second request to the actual device asking for info
                        String url = null;
                        try {
                            url = String.format("http://%s/device/", response.getJSONObject(0).getString("ip"));
                            mDeviceIp = response.getJSONObject(0).getString("ip");
                            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                                        @Override

                                        public void onResponse(JSONObject response) {
                                            // Make a second request to the actual device asking for info
                                            temp.setText(response.toString());
                                            displayControls(response);
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            return;

                                        }
                                    });
                            MySingleton.getInstance(context).addToRequestQueue(jsObjRequest);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        return;

                    }
                });
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);

    }

    private void displayDial(JSONObject service){
        final String serviceId;
        try {
            serviceId = service.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        LinearLayout layout = (LinearLayout) findViewById(R.id.control_main);
        RotaryKnobView knobView = new RotaryKnobView(this);
        layout.addView(knobView);
        knobView.setKnobListener(new RotaryKnobView.RotaryKnobListener() {
            // Local settings
            @Override
            public void onKnobChanged(int arg) {
                // Lookup current value and add one
                JSONObject currentService = serviceMap.get(serviceId);
                double currentValue;
                try {
                    if (currentService.getString("variableType") == "INT") {
                        currentValue = currentService.getJSONArray("settings").getJSONObject(0).getInt("currentValue");
                    } else if (currentService.getString("variableType") == "FLOAT") {
                        currentValue = currentService.getJSONArray("settings").getJSONObject(0).getDouble("currentValue");
                    } else {
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
                currentValue += arg > 0? 1 : -1;
                // Send a request to update to the current value
                try {
                    currentService.getJSONArray("settings").getJSONObject(0).put("currentValue", currentValue);
                    updateDeviceSettings(serviceId, currentService);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
            }
        });
    }

    private void updateDeviceSettings(String serviceId, JSONObject settings){
        String url = String.format("https://%s/device/service/%s/settings/", mDeviceIp, serviceId);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, settings, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Make a second request to the actual device asking for info

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    private HashMap<String, JSONObject> serviceMap = new HashMap<String, JSONObject>();

    private void displayControls(JSONObject description){
        //
        try {
            JSONArray lowServices = description.getJSONObject("services").getJSONArray("low");
            for (int i = 0; i < lowServices.length(); i++){
                JSONObject service = lowServices.getJSONObject(i);
                switch (service.getString("variableType")){
                    case "FLOAT":
                    case "INT":
                        // Add service to our hashmap, so we can lookup the current information
                        serviceMap.put(service.getString("id"), service);
                        // Generate a knob and dial for control
                        displayDial(service);
                        break;
                    case "BOOL":
                        break;
                    default:
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.control_layout);
        TextView urlView = (TextView) findViewById(R.id.control_url);
        Intent i = getIntent();
        String url = i.getStringExtra("url");
        urlView.setText(url);

        // Check registry for value
        requestDeviceInfo(urlView, url);
    }

}
