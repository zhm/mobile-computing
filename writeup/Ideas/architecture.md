# Architecture Proposal
I propose organizing the architecture into 2 distinct classes of objects, where objects in the same class are considered as equivalent to other when other classes interface with them.

* Class 1
	* Message Broker
	* Address Registry
* Class 2
	* Devices
	* Smart Devices
	* Smart Processes
	* Smartphones (a smart device with a full user interface)

It is important that, though these devices have different roles in the ecosystem, they interface with each other in the same way. This way, interactions can be decoupled from a brittle hierarchical structure. Clear hierarchical structure encourages storing global state outside of objects, prohibits interoperability with different systems (since those systems might be dependent on the hierarchy), and make the system vulnerable to foibles of implementation details. Since IoT devices are naturally described by an Object-Oriented pattern, we follow similar design principles here.

## Class 1

Objects in Class 1 are communication objects, they establish direct communication between devices by providing communication channels and device address lookup. Objects in Class 2 may use Class 1 objects in order change other object’s settings, collect data from those objects, and trigger actions on such objects.

## Class 2

Class 2 objects are devices that provide interfaces to their settings, publish data using Class 1 objects, and issue commands to other objects. The interface for these objects is highly general, though we fully expect objects in this class to implement very different roles in the ecosystem. Some of these objects might be ‘dumb’ sensors that merely report data, have very minor configuration settings, and do not store data locally. Other objects in this class may be more ‘smart’, they may store their own data, make decisions on the basis of data that they have collected from other Class 2 objects, issue commands to other objects, and make changes to their own configuration. The last important distinction for objects in this class is that they are not necessarily physical, they may be virtual processes running on a large machine, and not a dedicated device. These objects are treated no differently than other Class 2 objects, but we feel it should be made clear that these objects may be virtual.

###Interfaces
Elements in Class 2 implement 3 different interfaces:

* Low Services
* High Services
* Global Settings

####Control
Class 2 objects may control other Class 2 objects using their Low Services and the Settings interfaces.

Low Services are services that provide operations that are close to the hardware. For example, the following list is a set of values one might expose as a “Low Service”:
The current reading of sensor (i.e. the temperature, pressure, light level)
The current state of an actuator (i.e. a boolean describing whether a door is open or not)
Low Services make a guarantee to the caller that requests to change these values will be executed immediately, and that the state of the device will change exactly in accordance with the request. For example, a thermostat may expose a Low Service that allows another device to set the target temperature to 80 degrees. The thermostat promises that, on receiving a request, the device will raise the house’s temperature to 80 degrees as soon as possible.

Global settings are settings that are not directly tied to a change in physical state. For example, a Class 2 object may expose a Global Settings interface that allows other devices to change the sampling rate of a sensor.

Finally, Class 2 objects may expose High Services. High Services are different from Low Services in that they do not guarantee that the state of the device will be altered by the request. An example of a High Service is a service that ingests sound data, processes the data, and decides what color to set mood lighting too. The device makes no guarantees that any particular sound byte will make any particular change to the state of the device, so it is offered as a High Service.

####Communication
Class 2 objects may periodically publish eadings to a communication channel provided by a Class 1 object. These readings are accessible to other Class 2 objects that subscribe to that object’s channel. Messages published to the communication channel are not intended to be used for control (though they may be used as such). Instead the communication channel is offered as a way for other Class 2 object to consume data produced by an object, and issue commands accordingly.

Class 2 objects communicate with Class 1 objects in order to identify other Class 2 objects, and to discover channels on which those other Class 2 objects communicate.

The following figure provides an example of communication flow between objects:
![Architecture](MicroserviceModel.png)
