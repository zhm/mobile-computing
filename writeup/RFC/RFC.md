# An RFC for an IoT Communication Framework

Since protocols for describing network protocols are often described using an
RFC, we believe that an RFC-style description of our protocol will be an easily
digestible format.

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to be
interpreted as described in RFC 2119.

# Section 0 - Introduction

This document specifies a protocol that allows consumer-level Internet of Things
(IoT) devices to efficiently, effectively, and securely communicate over a home
network. While applications for commercial use are not explicitly within the
scope of this protocol, the protocol MAY be used for such applications. The
description of the protocol will be divided into 6 sections, Section 1 will
define the classes of objects used by the protocol. Section 2 will describe the
RECOMMENDED architectural support for the protocol. Section 3 will describe the
protocol. Section 4 will describe our implementation. Section 5 will describe
the security of our protocol. Section 6 concludes the specification with a
discussion of the problems addressed by the protocol.

## Section 1 - Object Definitions
### Classes

The protocol distinguishes two equivalence classes for objects that communicate
using the protocol:

* Class 1 (Commuication)
	* Message Broker
	* Address Registry
* Class 2 (Sensing, Actuation, and Planning)
	* Devices
	* Smart Devices
	* Smart Processes
	* Smartphones (a smart device with a full user interface)

The interface for objects in the same class SHOULD be consistent between
objects. Objects in Class 1 SHOULD be able to communicate with Objects in Class
2 using the same interface. Similarly an object in Class 2 SHOULD communicate
with other objects in Class 2 using the same interface.

Note that the choice to treat IoT devices as objects that participate in
equivalence relations is a carefully made design decision. IoT devices are often
_physical_ devices that are naturally defined as 'objects'. We believe this
makes an Object-Oriented (OO) design approach _the_ natural approach to IoT
device communication. One of the design paradigms of OO design is the use of
subclasses and function overriding to ensure that heterogeneous classes are able
to interface with each other in a consistent way. In order for IoT devices to
communicate effectively, we believe that devices must communicate with a
restricted but consistent set of interfaces regardless of their individual
capabilities. This allows devices to consume data and make decisions without too
much of their application logic being stored in the server (making the server
brittle). It also prevents individual instances from having to coordinate (on
their own) actions that affect global state (which leads to inconsistent state
and race conditions on decisions). The goal is to strike a balance between
placing too much application logic in one device, and too little application
logic in many devices.

### Class 1

Objects in this class are (broadly) communication facilitators. Objects in this
class SHOULD provide one or more of the following services:

* Message Brokering
	* Enable direct communication between devices using 1-to-many communication
	(A Publisher-Subscriber model is RECOMMENDED)
* Address Registration
	* Enable lookup of device addresses for direct 1-to-1 communication


Object in this class are intended to enable communication between different
devices, and are differentiated from other classes of objects for this purpose.
Service discovery is difficult to perform in a distributed fashion in
a distributed system, so communication objects bootstrap communication for the
system.

Objects in other classes MAY communicate with Class 1 objects in order to alter
other objects state, collect data from those objects, and trigger actions on
other objects.

### Class 2

Objects in this class are devices or software that provide a service, interface,
or aggregated information. Devices in this class MAY provide one or more of
the following services:

* Sensor readings of the environment
* Interfaces to trigger actuators in the environment
* Aggregation of information and a resource to it

Objects may implement more services than enumerated here. However, devices MUST
provide these services using the protocol defined in Section <!--TODO-->.

The interface for interacting with objects in Class 2 SHOULD be designed to
abstract the specifics of devices away from the interface. We believe that in
order to create a stable, and robust system, the specific details of each
device should be abstracted away as much as possible from device specific
considerations. However, we fully expect objects in Class 2 to have very
different roles in the ecosystem. Some of objects in the ecosystem might be
‘dumb’ sensors that merely report data, have very minor configuration settings,
and do not store data locally. Other objects in this class may be more ‘smart’ -
they may store their own data, make decisions on the basis of data that they
have collected from other Class 2 objects, issue commands to other objects, and
make changes to their own configuration.

### Note on Physical vs. Virtual "Devices"

It should be noted that objects in each class are not necessarily physical
objects. They may be virtual processes running on a large machine, rather than
on a dedicated device. These objects are treated no differently than physical
objects, but we feel it should be made clear that these objects may be virtual.


## Section 2 - Architecture

This section describes a system architecture for implementing the protocol. It
is given before the actual protocol so that it may be used as a reference. The
specific implementation here is NOT REQUIRED, but it is RECOMMENDED: we think
this architecture is a natural way to describe the protocol, but it is not
strictly REQUIRED.

<!--TODO: Image here for architecture-->

### Communication

A Class 1 object in the ecosystem SHOULD provide one or more of the following
forms of communication:

* 1-to-1 communication
* 1-to-many communication

1-to-1 communication SHOULD be implemented using a Client-Server model. The
network protocol that the communication is performed over MAY be implemented
using HTTP, HTTPS, or some other communication method (our method uses
HTTP/HTTPS)

1-to-many communication SHOULD be implemented using a Publisher-Subscriber
model. The specific network protocol used MAY be ZeroMQ, WAMP, MQTT, or another
protocol that provides a Publisher-Subscriber model.

### Registry

A Class 1 object in the ecosystem SHOULD provide a registration service that is
accessible to all devices in the ecosystem. The registration service MUST
accept a message (sent over either a 1-to-1 or a 1-to-many protocol) that
contains the following information:

* A unique identifier for a device
* The current status of a device
* The fully qualified URL of the device

In our implementation, the unique identifier for each device is generated
manually using a Python UUID generator. In production, this UUID SHOULD be
set by the manufacturer (much like a MAC address).

## Section 3 - The Protocol

As discussed in Section 1, Objects in each Class communicate using interfaces.
This section will describe the protocol that is used to implement those
interfaces. For 1-to-1 communication, the protocol (API) is designed to be
REST-ful. For 1-to-many communication the communication a publisher-subscriber
model is used.

### Interfaces

#### Class 1

Objects in Class 1 implement two different types of interfaces:

* A message broker based on a Publisher-Subscriber model
* A REST-ful registry that store connection information about objects

Objects in this Class 1 MUST be accessible to other objects over a discoverable
fully-qualified URL. Discovery of objects in Class 1 MAY be implemented using a
known static URL, but other methods MAY be used.

The message broker MUST expose an interface that allows Publisher and Subscriber
connections to be made to the database.

The REST-ful registry MUST expose a REST-ful API that consistently allows other
objects to query and upload the following information about devices:

* UUID
* Status
* Fully-qualified URL

UUIDs (Universal Unique Identifiers) SHOULD be generated by the manufacturer,
and MUST be globally unique to each device (much like a MAC address). The Status
field SHOULD be one of the following values:

* ONLINE
* BOOTING
* STANDBY
* BUSY
* MAINTENANCE
* ERROR
* FATAL_ERROR

'ONLINE' SHOULD be used as the default status for devices that are fully powered
on and capable of responding to requests immediately. 'BOOTING' SHOULD be used
for devices that have been powered on, but are not fully ready. 'STANDBY' SHOULD
be used for devices that are in a low-power state that are capable of responding
to requests, but no necessarily immediately. 'BUSY' SHOULD be used for devices
that are running at capacity and are unable / unwilling to accept new requests.
'MAINTENANCE' SHOULD be used for either device or user-initiated downtime where
the device is performing a maintenance task, but can still respond to requests.
'ERROR' SHOULD be used for recoverable errors that the device is currently
recovering from. 'FATAL_ERROR' SHOULD be used for when a device has encountered
an error from which is cannot continue.

Finally the fully-qualified URL field should be a field of the form
"<ip>:<port>" where ip is the IP address of the device and port is the
port on which the device is running a query-able server.

#### Class 2

Objects in Class 2 implement three different types of interfaces:

* Low Services
* High Services
* Global Settings

Low Services are services that provide operations that are "close to the
hardware". For example, a Low Service MAY expose interfaces for the following
types of data:

* The current reading of sensor (i.e. the temperature, pressure, light level)
* The current state of an actuator (i.e. a boolean describing whether a door is
open or not)

Low Services MUST guarantee that interactions over a Low Service correspond to a
1-to-1 change in device state. That is, requests successfully received by the
Low Service are guaranteed to affect device state in a way consistent with the
service settings. For example, a thermostat may expose a Low Service that allows
another device to set the target temperature to 80 degrees. The thermostat
promises that, on receiving a request, the device will raise the house’s
temperature to 80 degrees as soon as possible.

High Services are services that provide "high-level" operations where a 1-to-1
correspondence between requests sent to the API and the device state is
OPTIONAL. For example, a mood sensing application may expose a High Service that
ingests sound data, processes the data, and decides what color to set mood
lighting to, before making a request against devices that control the room
lighting. The device makes no guarantees that any particular sound byte will
make any particular change to the state of the device.

Mappings naturally describe the behavior of many High Services. For instance,
the mood sensing application takes in sound data and assigns it to a mood
according to some set of rules called an input mapping. Each mood also
corresponds to a light color according to an output mapping. Thus, a High
Service MAY accept an input mapping and/or output mapping from the user.

In practice, High Services are distinguished from Low Services by the level of
information and analysis done by the device exposing the service before
executing an action based on device state. High Services analyze and process
data independently, make a decision based on that analysis, and then act. Low
Services do minimal independent analysis. Additionally, High Services MAY
publish instructions to Low Services or directly modify their settings via
1-to-1 communication. In the above example, the final output of the mood sensing
High Service is to change the color settings of the lights which are Low
Services. In the case of the scheduling application demonstration, the
scheduler's final output is to publish a schedule to all connected devices
running Low Services for household tasks like laundry.

Global Settings are settings that affect all services running on a device, and
SHOULD be used to change device-global state. Global settings SHOULD NOT change
state that is specific to a single service (High or Low). For example, the
Global Settings interface might ask the device to reboot, set a system clock,
or alter the frequency of sensor readings (if this frequency is not set per
service).

#### Low Services

Low Services MUST implement for interfaces reading the following properties.

* UUID
* ServiceName
* ServiceNickName
* Type
* ReadingType
* Units
* Settings
* ModelInfo

Low Services MAY implement additional properties. Low Services MAY provide
interfaces for writing some of these properties. However, Low Services MUST NOT
provide interfaces for writing to the following properties:

* UUID
* ModelInfo

These fields are non-editable in a production environment. Development
environments may allow remote changes to these fields if devices are difficult
to change locally, however such functionality MUST be disable for production
environments.

##### UUID

A UUID is a universal unique identifier that is device-globally unique
identifer.

##### ServiceName

ServiceName SHOULD be a semi-human-readable name that may be used in URLs (for
example: barometerSensorService).

##### ServiceNickName
ServiceNickName SHOULD be a completely human-readable name (ex. Barometer Sensor
Service).

##### Type

Type MUST be one of two values: "SENSOR" or "ACTUATOR" (this distinction is made
as a hint to API users about device functionality. )

##### ReadingType

ReadingType MUST be a commonly used language type that can be understood by a typed
language (types supported availabe in the JSON format are RECOMMENDED). The
following types are REQUIRED, but more types MAY be added:

* BOOL
* INT
* FLOAT

ReadingType MUST also correspond to type of data published in the
Publisher-Subscriber channel associated with this service. It MAY also
correspond to the type of unit used in the settings field.

##### Units

Units MUST be a string representation of the units of measure that the service
provides data in. The value stored in this field must correspond to a
case-insensitive prefix code specified in Section 4 of
[The Unified Code for Units of Measure](unitsofmeasure.org/ucum.html).

##### Settings & ModelInfo Objects

Settings and ModelInfo MUST be a Settings and ModelInfo object respectively.
These objects are described in the next two sections (described in the next section)

#### Settings

Settings MUST be a object with the following set of properties:

* UUID
* Name
* Type
* CurrentValue
* Granularity
* Delay
* Units

UUID is defined in the same way as in previous sections.

Type is defined in the same way as ReadingType for Low Services proper

##### CurrentValue

CurrentValue MUST be the current value of this setting, stored in the
representation corresponding to TYPE.

##### Granularity

Granularity MUST be a value stored in a representation corresponding to TYPE.
This field SHOULD represent the level of precision that a device is able to
support in the CurrentValue field. For example, a thermostat may only be able to
maintain temperatures with a error of 1 degree Farenheight. The granuality
setting with Type = "INT" would be set to 1, meaning that the device can be
adjusted by no less than 1 unit of the type specified Units field (or the
service's Units field if no unit is specified in the setting (this SHOULD be
the case only when services that support exactly one setting.))

##### Delay

Delay MUST be an integer value specifying, in milliseconds, the amount of time
required between requests in order to guarantee accurate updates. For example,
this field could be set to 100ms, or the amount of time that must pass before
sending a another HTTP request. This field is useful for devices that cannot
cache HTTP requests, and MAY be set to 0 if delays are not expected, or caching
has been implemented on the device.

##### Units

Units MUST be a string representation of the units of measure that the service
provides data in. The value stored in this field must correspond to a
case-insensitive prefix code specified in Section 4 of
[The Unified Code for Units of Measure](unitsofmeasure.org/ucum.html).


#### ModelInfo

ModelInfo is an object that MUST specify the following fields:

* UUID
* Manufacturer
* ModelNumber
* SerialNumber

ModelInfo also MAY specify the following fields:

* DeviceDescription
* DeviceType

##### UUID

UUID MUST be a Earth-globally unique identifier for the the sensor or actuator
that the service provides access to.

##### Manufacturer

Manufacturer MUST be a string that identifies the manufacturer of the
sensor/actuator that this service provides access to. In the case of Virtual
Devices, this SHOULD be the developer of the software.

##### ModelNumber

ModelNumber MUST be a string that identifies the manufacturer's ModelNumber
corresponding to the sensor/actuator that this service provides access to. In
the case of Virtual Devices, this SHOULD be the major revision of the sofware.

##### SerialNumber

SerialNumber MUST be a string that identifies the serial number of the
sensor/actuator that this service provides access to. In the case of Virtual
Devices, this SHOULD be the minor revision number of the software.

##### Device Description

DeviceDescription SHOULD be a human-readable description of the sensor/actuator.
It will be used as a hint for user / developers interfacing with the API.

##### DeviceType

DeviceType SHOULD be a type specified in a broad category of devices. This field
should be used for standardizing device discovery.

#### High Services
High Services MUST implement interfaces reading the following properties.

* UUID
* ServiceName
* ServiceNickName
* Settings
* ModelInfo
* ServiceType
* CanAcceptCustom

High Services MAY implement interfaces reading the following properties.

* InputMappingType
* OutputMappingType
* UpdatedAt

High Services MAY implement interfaces writing the following properties.

* InputMapping
* OutputMapping

High Services MAY implement additional properties. High Services MAY provide
interfaces for writing some of these properties. However, High Services MUST NOT
provide interfaces for writing to the following properties:

* UUID
* ModelInfo

UUID, ServiceName, ServiceNickname, ModelInfo, and Settings are all identical to
their counterparts in Low Services.

##### ServiceType

ServiceType SHOULD be one of a broad category of services enabling the find
services matching a given type.

##### CanAcceptCustom

CanAcceptCustom MUST be a boolean indicating whether or not the High Service
offers customization of its input and output mapping.

##### UpdatedAt

This is an integer indicating the last time the High Service produced output.

##### InputMappingType and OutputMappingType

InputMappingType SHOULD indicate two commonly used programming language types
such as int->string which describe the data that is received and the data that
is output by the mapping. OutputMappingType is the same but describes the types
of the data produced by the High Service and the output of the mapping
respectively (for instance string->float)

##### InputMapping and OutputMapping

InputMapping and OutputMapping MUST indicate the actual mappings from one set of
values to another that correspond to the types defined in InputMappingType and
OutputMappingType.

#### Readings

Objects in Class 2 MAY also publish Readings over a communication channel
provided by a Class 1 object. Both High Services and Low Services may send
readings over the communication channel.

Readings MUST provide the following fields as part of messages sent over the
communication channel:

* Type
* Value

Readings MAY also provide the following fields:

* Min
* Max
* Timestamp
* Units

Readings are intended to inform other objects of work performed by the
publishing object that are intended for consumption. For example, a thermometer
device may publish messages about the current temperature as a Reading, or a
device intended it to analyze someone's mood, might publish an aggregation of
the data it has collected so that other devices could act on the analyzed mood.

##### Type, Timestamp, and Units
Type MUST be equivalent to the definition of ReadingType for LowServices.

Timestamp MUST be represented an ISO 8601 timestamp.

Units MUST be equivalent to the definition of Units in the Settings object.

##### Value

Value MUST be a value of the type specified by Type, and represented in a
corresponding format.

##### Min & Max

Min SHOULD be the minimum value of this reading. Not all readings have minimums,
(like JSON strings), but ones that do should use this field to give a hint to
the user about how far the data is from the minimum.

Similarly, Max SHOULD be the maximum value of this reading, and is provided so
that the user can compare the reading to maximum possible value.


## Section 4 - Notes on the Implementation of the Protocol

Section 3 described the requirements of interfaces that implement the protocol.
In this section we will discuss an implementation of the protocol. This
implementation is RECOMMENDED: it uses a combined Client-Server and
Publisher-Subscriber that naturally describes the protocol, but we recognize
that it may not cover all use cases. This implementation is provided as a tool
for understanding the protocol, nothing more or less.

Our implementation of the protocol requires each device in the network to
communicate using two interfaces: a Web Server (implemented on each device) and
a Message Broker (implemented on one device).

Objects communicate over a central message broker by publishing messages to a
"topic" (a subsection of the communication channel identified by a marker placed
on the message). Topics are labeled using the DeviceID and ServiceID that are
publishing the readings. For example, a thermometer (with the UUID:12345)
publishing temperature readings (as a service with ServiceID:789) would publish
the readings on unique channel labeled "[12345:789]". Objects "publish" these
messages over a ZeroMQ socket to another ZeroMQ socket acting as a message
broker. We implement the Message Broker in a standalone process
running on a central machine (the Server).

The Server also implements a separate process that acts as a registry service.
This services uses Flask and a RESTful API to provide an interface for other
objects to access registry entries.

The Server implements two Virtual Class 1 objects, a Registry and a Broker, and
may implement other Class 1 or Class 2 objects.

Each device (we can refer to them as Nodes) communicate with each other by
publishing messages to the Message Broker and by providing their own RESTful
interfaces. In our implementation, we primarily use Flask and it's RESTful
classes, but the Web Server can be implemented using any technology (even on
an Arduino!)

## Section 5 - Security

## Section 6 - Conclusion

We believe that a distributed architecture must carefully distribute the "smarts" across the connected devices. Centralizing too much knowledge of global state and control of other devices to a single server exposes a single point of failure and renders the system brittle. Correcting too far in the other direction deprives any devices of knowledge necessary for intelligent decision making and communication ability. We believe our protocol addresses these concerns and strikes an ideal balance.
