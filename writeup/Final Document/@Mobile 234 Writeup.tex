%%%%%%%%%%%%%%%%%%%%%%% file template.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is a general template file for the LaTeX package SVJour3
% for Springer journals.          Springer Heidelberg 2010/09/16
%
% Copy it to a new file with a new name and use it as the basis
% for your article. Delete % signs as needed.
%
% This template includes a few options for different layouts and
% content for various journals. Please consult a previous issue of
% your journal as needed.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% First comes an example EPS file -- just ignore it and
% proceed on the \documentclass line
% your LaTeX will extract the file if required
\begin{filecontents*}{example.eps}
%!PS-Adobe-3.0 EPSF-3.0
%%BoundingBox: 19 19 221 221
%%CreationDate: Mon Sep 29 1997
%%Creator: programmed by hand (JK)
%%EndComments
gsave
newpath
  20 20 moveto
  20 220 lineto
  220 220 lineto
  220 20 lineto
closepath
2 setlinewidth
gsave
  .4 setgray fill
grestore
stroke
grestore
\end{filecontents*}
%
\RequirePackage{fix-cm}
%
\documentclass{svjour3}                     % onecolumn (standard format)
%\documentclass[smallcondensed]{svjour3}     % onecolumn (ditto)
%\documentclass[smallextended]{svjour3}       % onecolumn (second format)
%\documentclass[twocolumn]{svjour3}          % twocolumn
%
\smartqed  % flush right qed marks, e.g. at end of proof
%

\usepackage{graphicx}
\usepackage[space]{grffile}
\usepackage{latexsym}
\usepackage{amsfonts,amsmath,amssymb}
\usepackage{url}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\hypersetup{colorlinks=false,pdfborder={0 0 0}}
\usepackage{textcomp}
\usepackage{longtable}
\usepackage{multirow,booktabs}
\usepackage[english]{babel}
%
% \usepackage{mathptmx}      % use Times fonts if available on your TeX system
%
% insert here the call for the packages your document requires
%\usepackage{latexsym}
% etc.
%
% please place your own definitions here and don't use \def but
% \newcommand{}{}
%
% Insert the name of "your journal" with
% \journalname{myjournal}
%


\bibliographystyle{spmpsci}


\begin{document}

\title{Experimental Standards for Seamless Communication Among IoT Devices
%\thanks{Grants or other notes
%about the article that should go on the front page should be
%placed here. General acknowledgments should be placed at the end of the article.}
}
%\subtitle{Do you have a subtitle?\\ If so, write it here}

%\titlerunning{Short form of title}        % if too long for running head

\author{Michael Zhao \and Justyn Harriman \and Hemanth Potluri}

\institute{Michael Zhao \at University of Chicago \and Justyn Harriman \at University of Chicago \and Hemanth Potluri \at University of Chicago}

%\authorrunning{Short form of author list} % if too long for running head

%\date{Received: date / Accepted: date}
% The correct dates will be entered by the editor

\maketitle





\section{Introduction}\label{introduction}

The age of the Internet of Things (IoT) will soon arrive. As major
companies, ranging from software makers to appliance giants, move to
usher in the era of smart, connected devices in every corner of the home
and office, we notice the same kind of ecosystem fragmentation that
characterized early computer networks, media distribution formats,
smartphone platforms, and wireless charging technologies. In the rush to
be the first to market or the best on the market, manufacturers must
either develop proprietary APIs for their IoT devices or embrace an open
standard. We believe the latter will allow the smart home to offer a
better integrated, less frustrating, and more affordable
experience---good for consumer adoption and satisfaction. We developed
guidelines for and demonstrations of an implementation of an ``API for
the IoT,'' which we present herein.


\section{Implementation}\label{implementation}


\subsection{Libraries used}\label{libraries-used}

ZeroMQ (message broker), Pyglet (multimedia), AVBin (media playback),
Flask (Web API framework), ZXing (barcode reader)


\subsection{Server-side Component}\label{server-side-component}

A full description of the protocol is found in the associated RFC.

Nodes in the architecture are divided into 2 distinct classes of
objects, where objects in the same class are considered as equivalent to
other when other classes interface with them.

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Class 1

  \begin{itemize}
  \itemsep1pt\parskip0pt\parsep0pt
  \item
    Message Broker
  \item
    Address Registry
  \end{itemize}
\item
  Class 2

  \begin{itemize}
  \itemsep1pt\parskip0pt\parsep0pt
  \item
    Devices
  \item
    Smart Devices
  \item
    Smart Processes
  \item
    Smartphones (a smart device with a full user interface)
  \end{itemize}
\end{itemize}

It is important that, though these devices have different roles in the
ecosystem, they interface with each other using a common interface. This
way, interactions can be decoupled from a brittle hierarchical
structure. Clear hierarchical structure encourages storing global state
outside of objects, prohibits interoperability with different systems
(since those systems might be dependent on the hierarchy), and make the
system vulnerable to foibles of implementation details. Since IoT
devices are naturally described by an Object-Oriented pattern, we follow
similar design principles here.


\subsubsection{Class 1}\label{class-1}

Objects in Class 1 are communication objects, they establish direct
communication between devices by providing communication channels and
device address lookup. Objects in Class 2 may use Class 1 objects in
order change other object's settings, collect data from those objects,
and trigger actions on such objects.


\subsubsection{Class 2}\label{class-2}

Class 2 objects are devices that provide interfaces to their settings,
publish data using Class 1 objects, and issue commands to other objects.
The interface for these objects is highly general, though we fully
expect objects in this class to implement very different roles in the
ecosystem. Some of these objects might be `dumb' sensors that merely
report data, have very minor configuration settings, and do not store
data locally. Other objects in this class may be more `smart', they may
store their own data, make decisions on the basis of data that they have
collected from other Class 2 objects, issue commands to other objects,
and make changes to their own configuration. The last important
distinction for objects in this class is that they are not necessarily
physical; they may be virtual processes running on a large machine,
rather than on a dedicated device. These objects are treated no
differently than other Class 2 objects, but we feel it should be made
clear that these objects may be virtual.


\subsubsection{Interfaces}\label{interfaces}

Elements in Class 2 implement 3 different interfaces:

\begin{itemize}
\itemsep1pt\parskip0pt\parsep0pt
\item
  Low Services
\item
  High Services
\item
  Global Settings
\end{itemize}


\subsubsection{Control}\label{control}

Class 2 objects may control other Class 2 objects using their Low
Services and the Settings interfaces.

Low Services are services that provide operations that are close to the
hardware. For example, the following list is a set of values one might
expose as a ``Low Service'':

\begin{itemize}
\item
  The current reading of sensor (i.e.~the temperature, pressure, light
  level)
\item
  The current state of an actuator (i.e.~a boolean describing whether a
  door is open or not)
\item
  Low Services make a guarantee to the caller that requests to change
  these values will be executed immediately, and that the state of the
  device will change exactly in accordance with the request. For
  example, a thermostat may expose a Low Service that allows another
  device to set the target temperature to 80 degrees. The thermostat
  promises that, on receiving a request, the device will raise the
  house's temperature to 80 degrees as soon as possible.
\end{itemize}

Global settings are settings that are not directly tied to a change in
physical state. For example, a Class 2 object may expose a Global
Settings interface that allows other devices to change the sampling rate
of a sensor.

Finally, Class 2 objects may expose High Services. High Services are
different from Low Services in that they do not guarantee that the state
of the device will be altered by the request.


\subsubsection{Communication}\label{communication}

Class 2 objects may periodically publish readings to a communication
channel provided by a Class 1 object. These readings are accessible to
other Class 2 objects that subscribe to that object's channel. Messages
published to the communication channel are not intended to be used for
control (though they may be used as such). Instead the communication
channel is offered as a way for other Class 2 object to consume data
produced by an object, and issue commands accordingly.

Class 2 objects communicate with Class 1 objects in order to identify
other Class 2 objects, and to discover channels on which those other
Class 2 objects communicate.

The following figure provides an example of communication flow between
objects:


\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.7\columnwidth]{figures/MicroserviceModel/MicroserviceModel.png}
\caption{Model of Communication
%
}
\end{center}
\end{figure}

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.7\columnwidth]{figures/ServicesUML/ServicesUML.png}
\caption{UML Diagram of the structure of devices and their services.
%
}
\end{center}
\end{figure}

\subsubsection{Phone App Details}\label{phone-app-details}

The phone app allows the user to control any connected device using a
simple and intuitive interface. The user scans a QR code corresponding
to the desired device. If multiple QR codes are present in the image it
will allow the user to select the correct one by tapping it. A set of
controls is then presented allowing the user to adjust the settings that
are available for the device.


\section{Challenges and Insights}\label{challenges-and-insights}


We learned how to enable an Android device to recognize multiple QR
codes simultaneously. We also learned how to write an API that was
lightweight enough to run on an Arduino (significant memory constraints)
but could also scale to control features as complicated as our Mood
Radio example. Because Arduinos run C++ code, we had to use an
implementation of the ZeroMQ wire protocol that functions as a PUSH/PULL
server. We learned how to implement this and how to then send the data
from the Arduino via a PUB/SUB model.


We used ZeroMQ to handle 1-to-many communication. ZeroMQ provides a
convenient Publisher/Subscriber system which allows our devices to
broadcast information to all interested parties. We used a REST API to
implement 1-to-1 communication. The API was able to query our registry
of devices and to get or adjust settings for a device. This was
implemented via the Flask library together with Flask-RESTful. Our
registry used sqlite to permanently store device IDs and statuses. To
implement barcode scanning for the phone app, we used the open source
ZXing library. We modified this significantly to allow it to handle
multiple QR codes in the view of the camera and labeling the recognized
codes with red dots. To handle music playing in the mood radio example
we used the pyglet library together with AVbin to allow it to handle
.mp3 files.


\subsection{Security and Performance}\label{security-and-performance}

Performance remains an interesting and unsolved problem for the Internet
of Things. In particular, API and software developers must accommodate
both the limited computational power of embedded devices and the
requirement for several different classes of nodes in an IoT-enabled
network to intercommunicate.

As the number of smart devices in the home, office, vehicle, and power
grid grows, the importance of securing the memory and communications of
these devices increases proportionately. IoT devices possess an attack
surface that combines those of embedded devices and network-connected
devices; this combination makes the Internet of Things especially
vulnerable to certain attack vectors.

Appliances and embedded devices have a relatively long lifespan compared
to PCs \cite{Babbitt_2009}. It is particularly important for these
long-lived devices to operate on a secure platform. Although we do not
address physical security, hardware- and firmware-level security, and
``secure programming'' as regards the security of the memory layout in
embedded software, we stress that these concerns should be a priority
for the engineers in charge of developing IoT devices. Because these
devices will often be used in a ``set and forget'' fashion, users must
be able to trust that their devices, and by extension their homes and
workplaces, will be reasonably secure from malicious actors throughout
their lifespan. If the device is manufactured with firmware updates in
mind, then there should be mechanisms in place to verify the
authenticity of any new firmware (cf.~Secure Boot).

Network connectivity poses the greatest threat to IoT devices. We expect
that in many implementations the devices will be connected (directly or
indirectly) to existing home networks and be vulnerable to attacks over
those networks. Vulnerabilities include unsecured and weakly secured
networks (e.g., WEP-encrypted WiFi networks), allowing for eavesdropping
and message modification; man-in-the-middle and spoofing attacks on
communications over insecure protocols, leading to misdirection or
unauthorized surveillance; denial-of-service attacks on important
network nodes, leading to critical service outages; infiltration of the
network by rogue devices added by untrusted parties; and modification of
the software running on a device after taking over or spoofing an
authority trusted by that device. Traditional remedies to these problems
include well-encrypted wireless communications (\emph{but,} designs must
account for IoT devices' limited processing power), a public-key
infrastructure with a trust chain originating at a user-designated root
(\emph{but,} this adds complexity and maintenance burdens), firewalls
(\emph{but,} careful not to trust the nodes inside the firewall
blindly), and tamper-evident software signing (\emph{but,} many
manufacturers do not sign their code).

Other network-related challenges include the difficulty of propagating
cryptographic keys to every device, which might be remedied by using a
trusted central authority to keep track of the keys in each deployment;
the generally slow performance of IoT-specific network protocols,
resulting in an intolerance for additional overhead; and the high packet
loss associated with wireless networking, requiring a tolerant but
performant message transport protocol to deal with dropped connections,
packet loss, and high-latency situations.

It is clear that, in an age where any and all personal information can
be used to identify and track a person's habits, the security of the
information being sent to and received from the devices in one's home or
workplace should be treated with care. To protect confidentiality,
encryption should be required for data in transit and in storage; AES is
a popular option, but as a symmetric cipher it requires both the sender
and recipient to share a key. To exchange keys, a protocol such as
Diffie-Hellman could be used to establish ephemeral session keys and
signing keys; RSA would also be suitable for signing and encryption.
Each party would need to hold distinct signing keys to prove their
identities and associated permissions, and a key broker would be
required to maintain an authoritative list of trusted devices. Message
integrity would also need to be guaranteed; this is often implemented
using HMAC with some hash algorithm. These problems have been addressed
in the PC world using primarily TLS, incorporating combinations of the
aforementioned cipher suites (e.g., ECDHE-RSA-AES-GCM-SHA), but to
implement the same on a wireless sensor device might impose an
insurmountable resource burden on the device. Yet, it is precisely these
devices that we would like to secure; beyond eavesdropping, it would be
trivial for an attacker to manipulate a smart home---open the garage
door, turn the thermostat to a wasteful or uncomfortable setting, or
activate an oven or fire alarm---without appropriate access controls in
place.

Because of the performance limitations of embedded devices, it is
difficult to design for confidentiality, integrity, and availability in
a protocol that must accommodate those devices. Popular
``maker-friendly'' devices, such as the Arduino Uno, have such limited
processing power and storage (16 MHz CPU, 2 KB SRAM, 32 KB flash memory)
as to be an impediment to implementing some widely-used encryption
protocols. Although the Arduino has been shown to run AES-128 at a
tolerable dozen-or-so kilobytes per second, other tests show that a
Diffie-Hellman key exchange could take several seconds to complete. The
performance of encryption and key exchange on embedded platforms is
highly dependent on the specific protocols used and their specific
implementations in terms of programming language (e.g., Assembly versus
C) and component algorithms (e.g., the Karatsuba algorithm can
theoretically multiply integers in
$\Theta(n^{\log_2{3}})$ steps, versus
$O(n^{2})$ for traditional multiplication methods, and mechanisms
that make use of operand caching are even faster)
\cite{hutter2014multiprecision}. Without careful attention to the
efficiency of the implementation, the code required for secure
communication could exceed the memory capacity of the embedded
device---even before any application code is added.

With more powerful devices it becomes easier to implement secure
protocols. For instance, we posit that any device powerful enough to run
a Linux-based operating system is generally powerful enough to use the
aforementioned cipher suite for all of its network communications
without significantly burdening its processor. On an IoT network, we
predict that devices of this kind will likely be tasked with data
aggregation and analysis from simpler, connected sensors. These smarter
devices are expected to have a useful amount of local storage and, as
such, should also protect that storage. Again, when the performance of
the device is not a severe impediment to encryption, it becomes easy to
implement all manner of encryption, signing, and hashing algorithms,
etc.; as long as the protocol is well designed, these measures will
greatly diminish the risk of surveillance and damage to the network and
its users.

At the level of a controller or ``root of trust'' device on an IoT
network, such a device should keep access control lists to authorize
some devices to communicate with others in specific ways; other devices
should receive approval from the trusted root before executing a
controlled action. The challenge then turns to authentication, both of
the human user of a commanding device and of the commanding device to
others on the network. For lack of a better solution, we propose a
``trust on first use'' device authentication mechanism, by which a human
user must confirm the identity of a device that wants to register and
join the existing network. For example, some smart home protocols
require the user to push a series of buttons on both the new device and
an existing commander. Perfect authentication of the human user's
identity remains an unsolved challenge; some combination of PINs,
passphrases, biometrics, and device-based authentication will likely be
used.

Security concerns for IoT devices are unlikely to diminish in importance
as time goes on; any protocol designed to power the Internet of Things
must place security in the highest priority, while also taking into
account the capabilities of the devices at hand.


\subsection{Hardware Limitations}\label{hardware-limitations}

We originally tried experimenting with a user interaction mode
incorporating a Google Glass headset and a smartwatch, allowing a user
to read a device's QR code---perhaps mounted on a wall---with the Glass
camera and then adjust the device's settings using gestures on the
smartwatch. We found that the combination of short battery life,
unreliable performance, and low camera resolution on the Glass was too
limiting to continue with this experiment, although we look forward to a
future in which this kind of interface will be easy to implement with
the available hardware. Our reference API implementation is ready to
receive such inputs.


\subsection{Changes We Would Make In
Retrospect}\label{changes-we-would-make-in-retrospect}

We would have diagrammed out the exact model for communication in detail
from the beginning. Different group members had different visions of how
the architecture would function. In particular, we debated exactly how
decentralized our system should be. On the one hand, an architecture
heavily centralized around a single server would enable the server to
track global state and perform the ``smart'' operations while leaving
the devices ``dumb'' and inexpensive. The more decentralized approach
enables devices to store their own settings and simplifies device-device
communication. Later, we were able to see that we could keep the design
mostly decentralized while while storing minimal information on the
(sometimes small) devices themselves. This was accomplished by allowing
the devices to publish data to a channel rather than storing it. It is
still possible to establish different hierarchies of devices on a
network using our protocol through mutual subscription: a controller
would subscribe to the information feeds of every device it controlled,
and \emph{vice versa} the controlled devices would subscribe to a
channel or channels published by the controller.


\subsection{Lessons Learned About Course
Themes}\label{lessons-learned-about-course-themes}

In attempting to build a system that enable IoT devices to communicate
with each other, we had to learn about how to design distributed
architectures. We gained a deeper understanding of how existing
ubiquituous computing systems that are intended for the home function.
We learned how to make devices of vastly different capabilities and
processing power work together. We also gained insight into why smarter,
more expensive, devices are often favored in IoT systems rather than the
cheap ubiquitous devices imagined by Weiser. We found that a very dumb
device can be hard to incorporate into the architecture. For instance,
our Arduino could not support the complete ZeroMQ library, and
implementing robust security was very challenging. However, a very smart
device that knows too much global state is brittle and not
generalizable. Devices like these are also expensive which is one of the
main problems facing mainstream adoption IoT. We also found that the
problem of dumb devices can be somewhat alleviated by allowing dumb
devices to talk to smarter devices. However, at a certain point, the
dumb device becomes more of a sensor attached to a smart device than a
smart device in its own right. Finally, we got a good look at what is
possible or feasible in today's market. Wearables are extremely
promising ubiquitous devices that could greatly increase the scope of
possible apps and provide for natural control of household IoT devices.
However, some current wearable technology is hampered by problems that
prevent widespread adoption as we learned through our tests with the
Google Glass. It seems that in today's market it is best to avoid
relying on them for IoT systems until the technology matures further.


\subsection{Low Service Example: Lamp}\label{low-service-example-lamp}

We used an Arduino Uno to emulate a smart device. It was connected to an
LED light which was our representation of a dimmable lamp. The lamp
offers a low service called brightness with an adjustable setting. This
setting can be adjusted via smart device. The user points the phone at
the QR code corresponding to the lamp and is then presented with a dial
allowing him to choose the appropriate brightness level.


\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.7\columnwidth]{figures/IMG_7740/IMG_7740.jpg}
\caption{%
}
\end{center}
\end{figure}

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.7\columnwidth]{figures/arduino_lamp__0002_7730_hand1/arduino_lamp__0002_7730_hand1.jpg}
\caption{After tapping the QR code, a control dial for the corresponding device
appears. When the dial is turned, a message is sent to the device over
the network to adjust the appropriate setting (in this case,
brightness). Both controller and lamp are made aware of the minimum and
maximum allowable brightness settings through our API. A cooperative
controller will give the user feedback when trying to exceed the
allowable range of values, and furthermore the software on the lamp will
ignore invalid inputs.
%
}
\end{center}
\end{figure}

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.7\columnwidth]{figures/arduino_lamp__0000_7729_low1/arduino_lamp__0000_7729_low1.jpg}
\caption{Dimmed light after decreasing brightness dial.
%
}
\end{center}
\end{figure}

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.7\columnwidth]{figures/arduino_lamp__0001_7728_high1/arduino_lamp__0001_7728_high1.jpg}
\caption{Light after increasing brightness dial.
%
}
\end{center}
\end{figure}

Moving the brightness dial sends a REST request defined by our API. In
our implementation, we process brightness changes every 100
milliseconds, allowing for a maximum of 10 adjustments per second.


\subsection{High Service Example: Mood
Radio}\label{high-service-example-mood-radio}

In this example we have a radio smart device represented by a laptop. It
offers a High Service called ``Mood Radio'' which allows it to pull data
from the current weather, the user's Google Calendar, and the time of
day as input information. It determines the user's mood based on this
data using an input mapping from input data to mood. Depending on the
device this can be user-specified. The High Service also has an output
mapping from mood to music genre. The radio will then play a random song
in the appropriate genre. When the current song ends, it will check to
see if the mood has changed before picking another song.


\subsection{Device-Device Communication Example: Task
Scheduler}\label{device-device-communication-example-task-scheduler}

This service allows various household devices such as a washing machine
to publish data including the duration of the job and dependencies on
other devices to a scheduling device. This scheduler intelligently
spreads out the tasks throughout the day so that if many houses have
similar systems, there are no spikes in electricity usage across the
city. It chooses appropriate times for each device to start work then
publishes the schedule back to the worker devices. This whole process is
initiated by a ``GO'' signal which could be generated automatically when
the user leaves his house for the day.


\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.7\columnwidth]{figures/1/1.JPG}
\caption{The terminal output of the scheduler device. The devices publish their
tasks and dependencies to the scheduler. The scheduler outputs a
schedule instructing the Washer to start at 9 AM and the dryer to start
at 1:15 PM.
%
}
\end{center}
\end{figure}

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.7\columnwidth]{figures/2/2.JPG}
\caption{The terminal output of a dryer device. The scheduler publishes the
schedule to the devices. This allows the devices to extract their start
time from the schedule using their ID and start work at the correct
time.
%
}
\end{center}
\end{figure}

\pagebreak

\section{Conclusion}\label{conclusion}

We were ultimately successful in designing a flexible API that can
handle both low-level manipulation of settings and high-level services
with optional user interaction. We were able to implement our API on
devices as simple as Arduinos and as complicated as fully-fledged
desktop computers. In the future we would like to continue to experiment
with improved phone-based user interfaces and alternative wearable-based
interfaces for devices. A combination of time constraints and
limitations of the hardware prevented us from implementing our vision of
enabling a user to control household devices by looking at them (with
Google Glass) and gesturing (with a smartwatch).


\bibliography{bibliography/converted_to_latex.bib%
}

\end{document}

