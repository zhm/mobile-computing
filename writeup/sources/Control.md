### Control

Class 2 objects may control other Class 2 objects using their Low Services and the Settings interfaces.

Low Services are services that provide operations that are close to the hardware. For example, the following list is a set of values one might expose as a “Low Service”:

* The current reading of sensor (i.e. the temperature, pressure, light level)

* The current state of an actuator (i.e. a boolean describing whether a door is open or not)

* Low Services make a guarantee to the caller that requests to change these values will be executed immediately, and that the state of the device will change exactly in accordance with the request. For example, a thermostat may expose a Low Service that allows another device to set the target temperature to 80 degrees. The thermostat promises that, on receiving a request, the device will raise the house’s temperature to 80 degrees as soon as possible.

Global settings are settings that are not directly tied to a change in physical state. For example, a Class 2 object may expose a Global Settings interface that allows other devices to change the sampling rate of a sensor.

Finally, Class 2 objects may expose High Services. High Services are different from Low Services in that they do not guarantee that the state of the device will be altered by the request.