## Idea
## Implementation
Please provide a detailed description of all applicable parts of your implementation.
### Libraries used
### Server-side component
### Phone or tablet app details
### Wearable device details, data sources, etc.
## Challenges, overcome and insurmountable:
### What did you learn?
### What approaches did you have to adopt?
### What proved too complext to implment or achieve?
## If you could do this project again from scratch, what would you do differently?
## What have you learned about the themes of the course through working on your project?
## Include screenshots that illustrate example usage scenarios of your app, step by step, with enough detail so that someone who was not at your demo could still fully appreciate what you have gotten up and running.

Although we tentatively planned to require a poster as well, you do not need to produce a poster.