## Idea
## Implementation
### Libraries used
### Server-side component- Implementation details: libraries used, server-side component, phone or tablet app details, wearable device details, data sources, etc. Please provide a detailed description of all applicable parts of your implementation.
- Challenges, overcome and insurmountable: what did you learn? what approaches did you have to adopt? what proved too complex to implement or achieve?
- If you could do this project again from scratch, what would you do differently?
- What have you learned about the themes of the course through working on your project?
- Include screenshots that illustrate example usage scenarios of your app, step by step, with enough detail so that someone who was not at your demo could still fully appreciate what you have gotten up and running.

Although we tentatively planned to require a poster as well, you do not need to produce a poster.