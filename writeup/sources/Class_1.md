### Class 1

Objects in Class 1 are communication objects, they establish direct communication between devices by providing communication channels and device address lookup. Objects in Class 2 may use Class 1 objects in order change other object’s settings, collect data from those objects, and trigger actions on such objects.