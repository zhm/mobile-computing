### Communication

Class 2 objects may periodically publish readings to a communication channel provided by a Class 1 object. These readings are accessible to other Class 2 objects that subscribe to that object’s channel. Messages published to the communication channel are not intended to be used for control (though they may be used as such). Instead the communication channel is offered as a way for other Class 2 object to consume data produced by an object, and issue commands accordingly.

Class 2 objects communicate with Class 1 objects in order to identify other Class 2 objects, and to discover channels on which those other Class 2 objects communicate.

The following figure provides an example of communication flow between objects: