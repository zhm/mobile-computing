## Libraries used

ZeroMQ (message broker), Pyglet (multimedia), AVBin (media playback), Flask (Web API framework), ZXing (barcode reader)