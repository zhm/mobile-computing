A number of companies have recently joined the Internet of Things and smart home movements. These include:

* Apple HomeKit: Integrated with other Apple products, such as Siri
* QIVICON: German, supported by a consortium of companies
* Belkin WeMo: IFTT integration