## Server-side Component

Nodes in the architecture are divided into 2 distinct classes of objects, where objects in the same class are considered as equivalent to other when other classes interface with them.

* Class 1
	* Message Broker
	* Address Registry
* Class 2
	* Devices
	* Smart Devices
	* Smart Processes
	* Smartphones (a smart device with a full user interface)

It is important that, though these devices have different roles in the ecosystem, they interface with each other using a common interface. This way, interactions can be decoupled from a brittle hierarchical structure. Clear hierarchical structure encourages storing global state outside of objects, prohibits interoperability with different systems (since those systems might be dependent on the hierarchy), and make the system vulnerable to foibles of implementation details. Since IoT devices are naturally described by an Object-Oriented pattern, we follow similar design principles here.