# Experimental Standards for Seamless Communication Among IoT Devices
* Justyn Harriman
* Hemanth Potluri
* Michael Zhao

Mobile Computing, Internet of Things, Protocol/API Design, Python/Flask, Arduino

## Project Structure

The project is structured into several components which we've separated by directory:

* The `rest_device` directory contains our code for starting devices and services.
    * Running app.py will start our flask API and some services.
	* Use `app.py radio` to run the Mood Radio demo
* The `rest_registry` contains code for a server that runs a device registry and a communication broker
* The `qr_multi_read` directory contains code for the Ubiquity Barcode Scanner App which presents controls for controlling a lamp (as well as a general interface for interacting with devices using our architecture.
* The `arduino_light_app` directory contains an Arduino project that represents a device interacting with our system using our protocol. The device provides a low service for turning on a "lamp" (or an LED sensor) in this case. The Arduino is capable of registering itself using the ZeroMQ wire protocol and responding to REST API requests. 
