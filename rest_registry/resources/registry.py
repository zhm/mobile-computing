### Python Built-ins ###
########################
import sqlite3
import time
import json

### Custom libraries ###
########################
from common.resource import DbResource

class Registry(DbResource):
    def get(self, device_id):
        res = self.cursor.execute('''SELECT * FROM Devices WHERE id=?;''', (device_id, ))
        names = [x[0] for x in res.description]
        return [{names[i]:x[i] for i in range(len(names))} for x in res.fetchall()]

class RegistryList(DbResource):
    def get(self):
        res = self.cursor.execute('''SELECT * FROM Devices;''')
        names = [x[0] for x in res.description]
        return [{names[i]:x[i] for i in range(len(names))} for x in res.fetchall()]

if __name__ == "__main__":
    Registry()
