### Microservices ###
#####################
from common.communication import Broker
from services.registry import RegistryReceive
from services.passthrough import PassPush

### Python built-ins ###
########################
import signal
import time
from multiprocessing import Process

if __name__ == "__main__":
    # Convenience array to start processes more easily.
    services = [Broker,RegistryReceive, PassPush]

    processes = []
    for service in services:
        newProcess = Process(target=service)
        processes.append(newProcess)
        newProcess.start()
        # After this point, we could check up on our processes restart them, if
        # they fail.

    # Sigint handler that will kill all processes when finished.
    def interrupt_handler(signal, frame):
        for process in processes:
            process.terminate()
        exit()

    signal.signal(signal.SIGINT, interrupt_handler)
    time.sleep(50)
