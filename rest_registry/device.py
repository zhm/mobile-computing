from flask import Flask, request, g
from flask_restful import Resource, Api, reqparse
from . import svc_config as config
import sqlite3
import zmq
import json
from communication import PubSub

'''def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(config.DB_PATH)
    return db
'''

class Device(Resource, PubSub):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        #self.reqparse.add_argument('id', type=str, location='json', required=False, help="Device ID required.")
        self.reqparse.add_argument('ipAddr', type=str, location='json', required=True, help="Device ID required.")
        self.reqparse.add_argument('status', type=str, location='json', required=True)
        #self.reqparse.add_argument('settings', type=dict, location='json', required=False)
        self.reqparse.add_argument('modelInfo', type=dict, location='json', required=True)
        super(Device, self).__init__()
        
    def post(self):
        return self.modify_device()

    def put(self):
        return self.modify_device()

    def modify_device(self):
        """
        Add (POST) or Modify (PUT) a device and its properties in the server's device registry.
        Input: POST data about the device: { "id":"IPv6 address", "status":"The current device status", "settings":{"setting1":"value1"[, ...]}, "modelInfo":{["info1":"value1"[, ...]]}}
        Output (normal): "True" if the device exists in the database, or "False" otherwise.
        Output (debug mode): A string representation of the database row corresponding to the device whose ID matches the requested one.
        POST: Add the new device to the server registry, or raise an error if a device with the same ID already exists.
        PUT: Modify an existing device and its properties in the server registry, or add an entry for the device if one does not already exist.
        """
        
        modification_type = "add" if request.method == "POST" else "edit" if request.method == "PUT" else None
        
        args = self.reqparse.parse_args()
        args["modification_type"] = modification_type
        print((json.dumps(args)))
        self.publish("registry", json.dumps(args))
        return True