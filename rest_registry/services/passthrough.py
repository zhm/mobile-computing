import time
import json

from common.communication import Pull, PubSub
import svc_config as config

class PassPush(Pull,PubSub):
    def __init__(self):
        Pull.__init__(self,host=config.SVC_HOST)
        PubSub.__init__(self,host=config.SVC_HOST)
        self.run()

    def parse(self, message):
        return message.decode("utf-8").partition("|||")

    def run(self):
        # Pass on the data to the Registry
        for received in self.listen():
            print(received)
            channel, _, message = self.parse(received)
            self.publish(channel, message)
