import time
import json

from common.communication import PubSub
from common.resource import DbObjectCreator

class RegistryReceive(DbObjectCreator, PubSub):
    def __init__(self):
        super(RegistryReceive, self).__init__()
        self.subscribe("registry")
        self.run()

    def createTables(self):
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS Devices (id TEXT PRIMARY KEY, status TEXT NOT NULL, ip TEXT);''')
        self.db.commit()

    def addDevice(self, data):
        data = json.loads(data)
        self.cursor.execute('''INSERT OR REPLACE INTO Devices VALUES (?, ?, ?);''', (data["id"], data["status"], data["ip"]))
        self.db.commit()
        return True

    def run(self):
        time.sleep(5)
        for message in self.listen():
            self.addDevice(message["data"])
