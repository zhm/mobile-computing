from flask import Flask, request, g
from flask_restful import Api
from resources.registry import Registry, RegistryList
import svc_config as config

app = Flask(__name__)
app.config['PROPAGATE_EXCEPTIONS'] = True
api = Api(app)

api.add_resource(Registry, '/registry/<device_id>/')
api.add_resource(RegistryList, '/registry/')

if __name__ == "__main__":
	app.run(host=config.SVC_HOST, port=config.SVC_PORT, debug=config.DEBUG_MODE)
