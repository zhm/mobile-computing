from flask import Flask, request, g
from flask_restful import Resource, Api, reqparse
import svc_config as config
import sqlite3

class DbResource(Resource):
    def __init__(self):
        self.db = sqlite3.connect(config.DB_PATH)
        self.cursor = self.db.cursor()
        super(DbResource, self).__init__()

class DbObjectCreator(object):
    def __init__(self):
        self.db = sqlite3.connect(config.DB_PATH)
        self.cursor = self.db.cursor()
        self.createTables()
        super(DbObjectCreator, self).__init__()

    # Abstract interface that must be implemented by the subclass
    def createTables(self):
        pass
